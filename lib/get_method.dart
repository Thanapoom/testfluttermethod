import 'package:flutter/services.dart';

typedef OnHandleSuccess = void Function(MethodCall);

class TestGoToSplashScreenHandle {
  MethodChannel methodChannel = MethodChannel("testPage");

  void handle({required OnHandleSuccess onHandleSuccess}) {
    methodChannel.setMethodCallHandler((call) async {
      if(call.method == getMethodName()){
        onHandleSuccess.call(call);
      }
    });
  }

  String getMethodName(){
    return "testPage";
  }

  void callNative() {
    methodChannel.invokeMethod("method","");
  }
}
