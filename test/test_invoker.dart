import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:testfluttermethod/mock_invoker.dart';

void main() {
  const MethodChannel _channel = MethodChannel('core.super_router');

  setUp(() async {
    TestWidgetsFlutterBinding.ensureInitialized();
  });

  test("call from native", () async {
    _channel.invokeMockMethod("something",null);
    // This call can't reach the handler in the Plugin
  });
}

